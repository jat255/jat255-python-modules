.. Copyright 2016 Joshua Taillon
..
.. Licensed under the Apache License, Version 2.0 (the "License");
.. you may not use this file except in compliance with the License.
.. You may obtain a copy of the License at
..
..     http://www.apache.org/licenses/LICENSE-2.0
..
.. Unless required by applicable law or agreed to in writing, software
.. distributed under the License is distributed on an "AS IS" BASIS,
.. WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
.. See the License for the specific language governing permissions and
.. limitations under the License.

Ellipsometry Thickness
======================

This package provides methods to plot and compute thickness and etch rates from ellipsometry measurements.

Basic instructions:

- Export thickness data from CompleteEASE (`Copy graph data to clipboard`)
- Save as a tab separated file
- Import data with `import_thicknesses()`
- Plot data with `plot_thicknesses()`
- Calculate differences....

Author: Joshua Taillon (jat255@gmail.com)