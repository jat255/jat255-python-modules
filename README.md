# README #

This repository serves as a reference to a collection of python modules that 
have been useful in my research, and perhaps they will be useful to you as well.

## Links to FIB/SEM specific modules: ##
* [FIBTracking](https://bitbucket.org/jat255/fibtracking/) - [![Documentation Status](https://readthedocs.org/projects/fibtracking/badge/?version=latest)](http://fibtracking.readthedocs.org/en/latest/?badge=latest) - Track FIB/SEM fiducial markers to measure slice thicknesses
* [FIBTortuosity](https://bitbucket.org/jat255/fibtortuosity/) - [![Documentation Status](https://readthedocs.org/projects/fibtortuosity/badge/?version=latest)](http://fibtortuosity.readthedocs.org/en/latest/?badge=latest) - Calculate geometric tortuosity of a 3D structure
* [FIBBootstrap](https://bitbucket.org/jat255/fibbootstrap) - [![Documentation Status](https://readthedocs.org/projects/fibbootstrap/badge/?version=latest)](http://fibbootstrap.readthedocs.org/en/latest/?badge=latest) - Bootstrap descriptive statistics from subvolumes of FIB/SEM data
* [tpbLen.py](https://bitbucket.org/jat255/tpb-scripts/overview) - Calculate triple phase boundary network for a 3D structure
* [TPBCalcs](https://bitbucket.org/jat255/jat255-python-modules/src/master/FIB-SEM/tpbcalcs) - Analyze connected components for expected electrochemical activity
* [Illumination Correction](https://bitbucket.org/jat255/illumination-correction-script) - Correct gradient intensities in FIB/SEM image stacks

## Other modules: ##
* [HyperSpy Tools](https://github.com/jat255/hyperspy_tools) - [![Documentation Status](https://readthedocs.org/projects/hyperspy-tools/badge/?version=latest)](http://hyperspy-tools.readthedocs.org/en/latest/?badge=latest) - Useful additional methods for use with [HyperSpy](http://www.hyperspy.org)
* [EllipsThickness](https://bitbucket.org/jat255/jat255-python-modules/src/master/XPS/ellipsthickness) - Some methods for plotting layer thickness maps from ellipsometry experiments

### How do I get set up? ###

* Summary of set up
    1. Clone the repository
    2. Move into specific module you want
    3. Install locally using `pip install -e ./`
* Dependencies
    * See each individual module for specific dependencies

### Who do I talk to? ###

* Josh Taillon (jat255@gmail.com)