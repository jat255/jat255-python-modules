import smtplib
import keyring

__version__ = '0.1.0'


class TextServer:
    """
    Class to send a message from a gmail address to an SMS gateway

    Adapted from Crake_'s library.

    To authenticate your email server and store the password in your local
    keyring, run the following before trying to send texts:

    >>> import keyring
    ... keyring.set_password('python_TextServer', emailaddress, password)

    .. _Crake: https://github.com/CrakeNotSnowman/Python_Message
    """

    def __init__(self,
                 address,
                 number,
                 carrier,
                 password=None):
        """
        Initialize a TextServer instance, and login to the gmail SMTP server

        Parameters
        ----------
        address: str
            email address from which to send the message
        number: str
            phone number to send the message to (e.g. 6018675309)
        carrier: str
            one of 'at&t', 'cricket', 'sprint', 'tmobile', 'uscellular',
            'verizon' or 'virgin'
        password: str or None
            password used to login to the email server. If None, will be
            read from the system keyring
        """
        self.gateways = {'at&t': 'txt.att.net',
                         'cricket': 'mms.mycricket.com',
                         'sprint': 'messaging.sprintpcs.com',
                         'tmobile': 'tmomail.net',
                         'uscellular': 'email.uscc.net',
                         'verizon': 'vtext.com',
                         'virgin': 'vmobl.com'}
        self.address = address
        self.password = password
        self.number = number
        self.carrier = carrier
        self.email = self.number + '@' + self.gateways[self.carrier]
        self.server = smtplib.SMTP("smtp.gmail.com", 587)
        self.login()

    def __str__(self):
        return "textServer instance sending messages from " + self.address

    def login(self):
        """
        Login to the SMTP server for sending emails
        """
        self.server.starttls()
        if self.password is None:
            self.password = keyring.get_password('python_TextServer',
                                                 self.address)
        else:
            pass
        self.server.login(self.address, self.password)

    def send_message(self, mess):
        """
        Send mess (a string) to the SMS gateway through self.server
        """
        self.server.sendmail(self.address, self.email, mess)
