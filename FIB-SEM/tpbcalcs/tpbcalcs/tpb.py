# Copyright 2016 Joshua Taillon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__version__ = '0.1.0'

import os
import sys

import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import libtiff
from progressbar import ProgressBar, Percentage, Bar, ETA
from PyQt4 import QtGui, QtCore


# noinspection PyTypeCheckerInspection
def gui_open_fname(title=None, directory=None):
    """
    Select a file to open via a dialog and return the file name.

    Parameters
    ----------
    title: str or None
        String to use as title of file chooser window
    directory: str or None
        Directory to use as the starting directory for the file chooser

    Returns:
    --------
    fname: str or list
        single string or list of strings representing file(s) that were
        selected using the file chooser dialog
    """
    if title is None:
        title = 'Select 3D tif file'
    if directory is None:
        directory = './'

    app = QtGui.QApplication.instance()
    if app is None:
        app = QtGui.QApplication(sys.argv)

    fname = QtGui.QFileDialog.getOpenFileName(None,
                                              title,
                                              directory,
                                              filter='All files (*)')
    return fname


# noinspection PyTypeCheckerInspection
def gui_save_fname(title=None, directory=None):
    """
    Select a file to save to via a dialog and return the file name.

    Parameters
    ----------
    title: str or None
        String to use as title of file chooser window
    directory: str or None
        Directory to use as the starting directory for the file chooser

    Returns:
    --------
    fname: str or list
        single string or list of strings representing file(s) that were
        selected using the file chooser dialog
    """
    if title is None:
        title = 'Select 3D tif file'
    if directory is None:
        directory = './'
    fname = QtGui.QFileDialog.getSaveFileName(None,
                                              title,
                                              directory,
                                              filter='All files (*)')
    return fname


def open_tif(input_file=None):
    """
    Open a tif file using libtiff and return some information about it

    Parameters
    ----------
    input_file: str
        file to be opened

    Returns:
    --------
    data_tif: libtiff.TiffFile
        TiffFile object to be used for further analysis
    ifd: libtiff.IFD
        IFD that holds information about the tiff, like the image
        description that can be extracted later
    """
    if input_file is None:
        input_file = gui_open_fname()

    # Load 3D tif file
    data_tif = libtiff.TiffFile(input_file)

    # Get information from Avizo-saved tiff file
    ifd = data_tif.get_first_ifd()

    return data_tif, ifd

def cc_analysis(input_file=None):
    """
    Analyzes edge-voxel containing connected components to see whether they
    exist on two edges (ACROSS), or if they are just on one edge (DEADEND).

    Parameters:
    -----------
    input_file: str or None
        this should be the path to a 3D tiff that contains only the border
        particles. That is, one that has had the borderkill isolated data
        subtracted out (improves speed)

    Returns:
    --------
    classified_data: numpy array
        array containing two types of labels. Where 1, the CC is "across"
        and should be active. Where 2, the CC is a "deadend" and is of
        unknown activity
    image_description: str
        the string that describes the image saved by Avizo for easy loading
        back into Avizo
    """
    # Load tif file:
    data_tif, ifd = open_tif(input_file=input_file)

    # Load 3D tif file
    data_array = data_tif.get_tiff_array()
    data = data_array[::].astype('int16')

    # Get information from Avizo-saved tiff file
    size_x = ifd.get_value('ImageWidth')
    size_y = ifd.get_value('ImageLength')
    size_z = data_tif.get_depth()
    image_description = ifd.get_value('ImageDescription')

    # Calculate some parameters from the image input
    bb_xmin, bb_xmax, bb_ymin, bb_ymax, bb_zmin, bb_zmax = \
        [eval(i) for i in image_description.split()[1:]]
    bound_x = bb_xmax - bb_xmin
    bound_y = bb_ymax - bb_ymin
    bound_z = bb_zmax - bb_zmin
    vox_x = (bb_xmax - bb_xmin) / float(size_x - 1)
    vox_y = (bb_ymax - bb_ymin) / float(size_y - 1)
    vox_z = (bb_zmax - bb_zmin) / float(size_z - 1)
    top_x, top_y, top_z = int(bound_x / vox_x), int(bound_y / vox_y), int(
        bound_z / vox_z)

    # Set up classification array
    boundaries = np.zeros(data.max())
    classified_data = np.zeros_like(data)

    # Setup progressbar
    widgets = ['Classifying edges: ', Percentage(), ' ', Bar(), ' ',
                       ETA()]
    progress = ProgressBar(widgets=widgets)

    # Classification constants
    ACROSS = 1
    DEADEND = 2

    # Loop across the range of index (label) values
    for i in progress(range(1,data.max())):
        # mask off where the data is equal to this index value
        mask = data == i
        borders = np.zeros(6)
        # check each of the 'where' arrays for boundary voxels (0 or top_i)
        borders[0] = np.any(mask[0, :, :])
        borders[1] = np.any(mask[top_z, :, :])
        borders[2] = np.any(mask[:, 0, :])
        borders[3] = np.any(mask[:, top_y, :])
        borders[4] = np.any(mask[:, :, 0])
        borders[5] = np.any(mask[:, :, top_x])

        # Check how many borders we intersect
        boundaries[i] = int(borders.sum())

        # classify across:
        if boundaries[i] > 1:
            classified_data[mask] = ACROSS

        # classify deadend
        if boundaries[i] == 1:
            classified_data[mask] = DEADEND

    return classified_data, image_description


def save_as_tif(classified_data,
                image_description,
                output_file=None):
    """
    Save data as a 3D tif that can be opened by Avizo

    Parameters
    ----------
    classified_data: numpy.array
        data that should be written to a tif file. Should be in numpy order
        (z,y,x)
    image_description: str
        Contains the information needed by Avizo to read in bounding box
        size, etc.
    output_file: str or None
        If None, a dialog box will be opened to show choose a file to save
        to. If a string, data will be written to that file.
    """
    if output_file is None:
        output_file = gui_save_fname()

    tiff = libtiff.TIFFimage(classified_data.astype('uint8'),
                             description=image_description)
    tiff.write_file(output_file,
                    compression='lzw')

if __name__ == "__main__":
    cc_analysis(int(sys.argv[1]))
