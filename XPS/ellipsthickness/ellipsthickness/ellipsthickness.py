# Copyright 2016 Joshua Taillon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__version__ = '0.1.0'

import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import scipy.interpolate
import scipy.stats
import seaborn as sns


def mean_confidence_interval(data, confidence=0.95):
    """
    Calculate data mean and confidence interval based on standard error of the mean

    data: list
        list of data
    confidence: float
        confidence interval (0 < CI < 1)

    Returns:
    --------
    tuple of mean, confidence interval, mean - CI, and mean + CI
    """
    a = 1.0 * np.array(data)
    # noinspection PyTypeChecker
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    # noinspection PyProtectedMember
    h = se * scipy.stats.t._ppf((1 + confidence) / 2., n - 1)
    return m, h, m - h, m + h


def readdata(fname,
             delimiter='\t',
             headerrows=2,
             **kwargs):
    """
    Read data from txt file and return numpy array
    Data is expected to be in the form:

        ## Header (any number of rows)
        <x location>    <y location>    <z data>

    fname: str
        filename to read data from
    delimiter: str
        separator of file
    headerrows: int
        number of rows to skip at beginning of file
    **kwargs:
        other arguments passed to np.genfromtxt()
    """
    data = np.genfromtxt(fname,
                         delimiter=delimiter,
                         skiprows=headerrows,
                         **kwargs)
    return data


def plot_thickness_map(data,
                       xy_units='cm',
                       z_units='nm',
                       z_label='Film thickness',
                       sample_name=None,
                       ci=0.95,
                       cmap='RdBu_r',
                       cbar_ticks=5,
                       sample_xsize=None,
                       sample_ysize=None,
                       interpolationpoints=100,
                       interpolationfunction='linear',
                       markersize=70,
                       markertype='o',
                       markeredgecolor='black',
                       markerlinewidth=1,
                       sns_style='white',
                       sns_context='poster',
                       sns_kw=None,
                       ):
    """
    Plot an image with thickness values interpolated between given points

    data: numpy array
        Values to plot (read in with readdata() function)
    xy_units: str
        units of spatial position
    z_units: str
        units of thickness
    z_label: str
        label to use for colorbar (units will be added)
    sample_name: str
        sample name that will be used as plot title. If None, no title is set.
    ci: float
        confidence interval to report for mean +- error
    cmap: str
        matplotlib colormap to plot image with
        default is 'RdBu_r'
    cbar_ticks: int
        number of ticks to include on colorbar
    sample_xsize: tuple
        (xmin, xmax) values of sample bounds (in xy_units)
        Defaults to (0, 1)
    sample_ysize: tuple
        (ymin, ymax) values of sample bounds (in xy_units)
        Defaults to (0, 1)
    interpolationpoints: int
        number of points to use for interpolation grid
    interpolationfunction: str
        type of interpolation function to be used by `scipy.interpolate.Rbf`
        options are as follows, but 'linear' appears best for this application
            'multiquadric': sqrt((r/self.epsilon)**2 + 1)
            'inverse': 1.0/sqrt((r/self.epsilon)**2 + 1)
            'gaussian': exp(-(r/self.epsilon)**2)
            'linear': r
            'cubic': r**3
            'quintic': r**5
            'thin_plate': r**2 * log(r)
    markersize: float
        size of marker to use in scatter plot points
    markertype: str
        type of marker to use in scatter plot points
        e.g. 'o', 'x', 'D', '^', etc.
    markeredgecolor: str
        color of outer line in scatter plot points
    markerlinewidth: float
        width of outer line in scatter plot points
    sns_style: str
        default style for seaborn ('white', 'dark', 'darkgrid', etc.)
    sns_context: str
        context for plotting with seaborn
    sns_kw: dict
        additional arguments to be passed to seaborn.set_style
        (see http://goo.gl/WvLdc6 for details)

    ------
    Returns: matplotlib figure, <matplotlib axes>
        Handle to figure that is plotted (and axes array if subplots)
    """

    matplotlib.rcParams['mathtext.fontset'] = 'stixsans'

    if sns_kw is None:
        sns_kw = {'legend.frameon': True}

    sns.set_style(sns_style, sns_kw)
    sns.set_context(sns_context)
    sns.set_style({'image.cmap':cmap})
    sns.set_palette(cmap)

    # Set sample sizes
    if sample_xsize is None:
        xmin, xmax = 0, 1
    else:
        xmin, xmax = sample_xsize
    if sample_ysize is None:
        ymin, ymax = 0, 1
    else:
        ymin, ymax = sample_ysize

    if xy_units is 'um':
        xy_units = "\mu m"

    # Unpack data into distinct variables:
    x, y, z = data[:, 0], data[:, 1], data[:, 2]

    # Calculate some statistics on the data
    mu, err, _, _, = mean_confidence_interval(z, confidence=ci)

    # Set up grid of interpolation points
    xi, yi = np.linspace(x.min(), x.max(), interpolationpoints), np.linspace(y.min(), y.max(), interpolationpoints)
    xi, yi = np.meshgrid(xi, yi)

    # Do interpolation:
    rbf = scipy.interpolate.Rbf(x, y, z, function=interpolationfunction)
    zi = rbf(xi, yi)

    # Plot image and interpolated background
    fig, ax = plt.subplots()
    im = ax.imshow(zi,
                   vmin=zi.min(),
                   vmax=zi.max(),
                   origin='lower',
                   extent=[xmin, xmax, ymin, ymax],
                   )

    # Plot actual measured data and set bounds
    ax.scatter(x, y, c=z,
               s=markersize,
               marker=markertype,
               edgecolor=markeredgecolor,
               lw=markerlinewidth,
               vmin=zi.min(),
               vmax=zi.max(),
               )
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)

    # Set x and y labels
    ax.set_xlabel('x (' + xy_units + ')')
    ax.set_ylabel('y (' + xy_units + ')')

    if sample_name is None:
        ax.set_title('$\overline{\Delta f}$ = %.2f $\pm$ %.2f ' % (mu, err) + z_units)
    else:
        ax.set_title(sample_name + ': $\overline{\Delta f}$ = %.2f $\pm$ %.2f ' % (mu, err) + z_units)

    # Add colorbar for reference
    cax = fig.colorbar(im, ax=ax)
    cax.set_ticks(np.linspace(zi.min(),zi.max(),cbar_ticks))
    cax.set_ticklabels(['%.1f' % i for i in np.linspace(zi.min(),zi.max(),cbar_ticks)])
    cax.set_label(z_label + ' (' + z_units + ')')

    if sample_name is not None:
        fig.canvas.set_window_title(sample_name + ': ' + z_label)
    
    fig.show()

    return fig, ax


# def plot_thicknesses(thicknesses,
#                      lowess_factor=0.05,
#                      axes_limits='auto',
#                      units='nm',
#                      xlabel='Slice #',
#                      ylabel='auto',
#                      plot_data=True,
#                      dataplotkw=None,
#                      plot_lowess=True,
#                      lowessplotkw=None,
#                      plot_mean=True,
#                      meanplotkw=None,
#                      plot_std=True,
#                      stdplotkw=None,
#                      plot_legend=True,
#                      legend_loc='best',
#                      sns_style='white',
#                      sns_context='poster',
#                      sns_cmap=None,
#                      sns_color_data=1,
#                      sns_color_lowess=2,
#                      sns_color_mean=3,
#                      sns_color_std=4,
#                      sns_style_kw=None,
#                      ):
#     """
#     Plots a visualization of slice thickness measurements, as well as descriptive data
#
#     thicknesses: list
#         thickness data to be plotted
#     lowess_factor: float
#         factor to be used when smoothing thickness data for plotting (valid values are [0-1])
#         the higher the factor used, the more smoothed the data will be
#     axes_limits: 'auto' or list of int
#         axes limits to use when displaying the data. If 'auto', the limits will be determined from the data;
#         otherwise, should be given in the order [xmin, xmax, ymin, ymax]
#     units: str
#         units that values are given in (default of 'nm')
#     xlabel: str
#         label to use for the x-axis
#     ylabel: str
#         label to use for the y-axis
#         if 'auto', label will be built including the units
#     plot_data: bool
#         Switch to control whether or not raw data is plotted
#     dataplotkw: dict or None
#         dictionary containing additional keyword arguments for the data scatterplot
#         default included parameters are marker, linewidths, and label, which are set in the code if the value is None
#     plot_lowess: bool
#         Switch to control whether or not smoothed data is plotted
#     lowessplotkw: dict or None
#         dictionary containing additional keyword arguments for the LOWESS regression line plot
#         default included parameters are linewidth. label is defined in the code (if not given here)
#         defaults are set in the code if the value is None
#     plot_mean: bool
#         Switch to control whether or not data mean is plotted
#     meanplotkw: dict or None
#         dictionary containing additional keyword arguments for the mean line plot
#         default included parameters are lw. label is defined in the code (if not given here)
#         defaults are set in the code if the value is None
#     plot_std: bool
#         Switch to control whether or not data std dev is plotted
#     meanplotkw: dict or None
#         dictionary containing additional keyword arguments for the std dev line plots
#         default included parameters are lw and ls. label is defined in the code (if not given here)
#         defaults are set in the code if the value is None
#     plot_legend: bool
#         Switch to control whether or not legend is shown on the plot
#     legend_loc: str or int
#         see http://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.legend for details
#         Desired position for the legend
#     sns_style: str
#         default style for seaborn ('white', 'dark', 'darkgrid', 'whitegrid')
#     sns_context: str
#         context for plotting with seaborn
#     sns_cmap: list, str, tuple, or None
#         colormap for use with seaborn
#         default is ["4C72B1", "#004040", "#023FA5", "#8E063B", "#098C09", "#EF9708"]
#                   light blue, dark teal,      blue,       red,     green,    orange
#         If str or tuple, then these parameters are passed to the seaborn.color_palette() function
#         default cmap is set in the code if the value is None
#     sns_color_data: int
#         color (# from cmap) to use for plotting the thickness data
#     sns_color_lowess: int
#         color (# from cmap) to use for plotting the lowess-smoothed data
#     sns_color_mean: int
#         color (# from cmap) to use for plotting the data mean
#     sns_color_std: int
#         color (# from cmap) to use for plotting the data std
#     sns_style_kw: dict or None
#         additional arguments to be passed to seaborn.set_style
#         defaults are set in the code if the value is None
#         (see http://goo.gl/WvLdc6 for details)
#
#     Returns
#     -------
#     fig = Handle to figure that is plotted
#     """
#     import statsmodels.api as sm
#     matplotlib.rcParams['mathtext.fontset'] = 'stixsans'
#
#     # Checks on axes_limits parameter:
#     if axes_limits is 'auto':
#         pass
#     elif len(axes_limits) is not 4:
#         print "Specified axes_limits should be length 4. Resetting to \'auto\'."
#         axes_limits = 'auto'
#     elif type(axes_limits) is not list:
#         print "Specified axes_limits should be a list or \'auto\'. Resetting to \'auto\'."
#         axes_limits = 'auto'
#     elif not all([isinstance(x, (int, long, float)) for x in axes_limits]):
#         print "Specified axes_limits should be a list of numbers of length 4. Resetting to \'auto\'."
#         axes_limits = 'auto'
#
#     # Defaults for mutable type arguments
#     if dataplotkw is None:
#         dataplotkw = {'s':40, 'marker':'o', 'linewidths':0.3, 'label':'Measured slice thickness'}
#     if lowessplotkw is None:
#         lowessplotkw = {'linewidth':2}
#     if meanplotkw is None:
#         meanplotkw = {'lw':2}
#     if stdplotkw is None:
#         stdplotkw = {'lw':2, 'ls':'dashed'}
#     if sns_cmap is None:
#         sns_cmap = ["#4C72B1", "#004040", "#023FA5", "#8E063B", "#098C09", "#EF9708"]
#     if sns_style_kw is None:
#         sns_style_kw = {'legend.frameon': True}
#
#     # Import and setup seaborn styles
#     import seaborn as sns
#     matplotlib.rcParams['mathtext.fontset'] = 'stixsans'
#     sns.set_style(sns_style, sns_style_kw)
#     sns.set_context(sns_context)
#     if type(sns_cmap) is tuple:
#         palette = sns.color_palette(*sns_cmap)
#     else:
#         palette = sns.color_palette(sns_cmap)
#     sns.color_palette(palette)
#
#     fig = plt.figure()
#
#     x = range(len(thicknesses))     # range (x values) for the slice numbers
#     y = thicknesses                 # actual measured thickness data
#     i = lowess_factor               # factor by which to smooth the data
#
#     if plot_data:
#         if 'label' not in dataplotkw:
#             dataplotkw['label'] = 'Measured slice thickness'
#         plt.scatter(x, y, c=palette[sns_color_data], **dataplotkw)
#     if plot_lowess:
#         lowess = sm.nonparametric.lowess(y, x, frac=i)
#         if 'label' in lowessplotkw:
#             print lowessplotkw['label']
#         if 'label' not in lowessplotkw:
#             lowessplotkw['label'] = 'LOWESS regression ($f=' + repr(i) + '$)'
#         plt.plot(lowess[:, 0], lowess[:, 1], color=palette[sns_color_lowess], **lowessplotkw)
#     if plot_mean or plot_std:
#         thick_mean = np.mean(thicknesses)
#         thick_std = np.std(thicknesses)
#     if plot_mean:
#         if 'label' not in meanplotkw:
#             meanplotkw['label'] = '$\mu = ' + repr(round(thick_mean,2)) + '$ (' + units + ')'
#         plt.axhline(thick_mean, color=palette[sns_color_mean], **meanplotkw)
#     if plot_std:
#         if 'label' not in stdplotkw:
#             stdplotkw['label'] = '$\mu \pm \sigma = (' + repr(round(thick_mean - thick_std,2)) + ', ' + \
#                                  repr(round(thick_mean + thick_std,2)) + ')$ (' + units + ')'
#         plt.axhline(thick_mean - thick_std, color=palette[sns_color_std], **stdplotkw)
#         del stdplotkw['label']
#         plt.axhline(thick_mean + thick_std, color=palette[sns_color_std], **stdplotkw)
#
#     if plot_legend:
#         plt.legend(loc=legend_loc)
#
#     if ylabel is 'auto':
#         ylabel = 'Measured slice thickness (' + units + ')'
#     plt.ylabel(ylabel)
#     plt.xlabel(xlabel)
#
#     # Set limits of axes
#     if axes_limits is 'auto':
#         plt.axis([0,len(x),-5,1.2 * np.max(y)])
#     else:
#         plt.axis(axes_limits)
#
#     return fig


# def save_output(data,
#                 fname='slice_thicknesses_fibtracking.csv',
#                 overwrite=False,
#                 collabels='auto',
#                 units='nm',
#                 delimiter=',',
#                 print_time_stamp=True,
#                 ):
#     """
#     Write the output contained in a list to a csv file
#
#     data: list
#         list containing values to save (usually slice thicknesses)
#     fname: str
#         string containing name of file to which to write
#     overwrite: bool
#         if fname exists, switch to control whether to overwrite
#     collabels: list, 'auto', or None
#         labels to write as column headers. If 'auto', default of "Slice, Thickness (units)"
#         will be used. If a list, use str values separated by commas.
#     units: str
#         units to write in output
#     delimiter: str
#         delimiter to use in output
#     print_time_stamp: bool
#         switch to control whether a time stamp is printed on the first line of the file
#     Returns:
#         None
#     """
#     import csv
#     import time
#     import datetime
#     import os.path
#
#     if os.path.exists(fname) and not overwrite:
#         raise IOError("File exists, please choose different name or use \'overwrite=True\'.")
#
#     ts = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
#
#     if collabels is 'auto':
#         collabels = ['Slice', 'Thickness (' + units + ')']
#     elif type(collabels) is list:
#         pass
#     elif collabels is None:
#         pass
#     else:
#         print "Did not understand column label input. Using default values."
#         collabels = ['Slice', 'Thickness (' + units + ')']
#
#     with open(fname, 'wb') as csvfile:
#         writer = csv.writer(csvfile, delimiter=delimiter,
#                             quotechar='|', quoting=csv.QUOTE_MINIMAL)
#         if print_time_stamp:
#             csvfile.write(ts + '\n')
#
#         if collabels:
#             writer.writerow(collabels)
#
#         for i, x in enumerate(data):
#             writer.writerow([repr(i), repr(x)])
#
#         print ts
#         print "Wrote output to " + fname


if __name__ == "__main__":
    pass